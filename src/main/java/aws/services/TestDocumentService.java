package aws.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import aws.services.cloudsearchv2.AmazonCloudSearchClient;
import aws.services.cloudsearchv2.documents.AmazonCloudSearchAddRequest;
import aws.services.cloudsearchv2.documents.AmazonCloudSearchDeleteRequest;
import aws.services.cloudsearchv2.search.AmazonCloudSearchQuery;
import aws.services.cloudsearchv2.search.AmazonCloudSearchResult;
import aws.services.cloudsearchv2.search.Hit;
import com.amazonaws.auth.BasicAWSCredentials;

import javax.xml.bind.DatatypeConverter;

public class TestDocumentService {

    AmazonCloudSearchClient client = null;

    private static final String AWS_ENCR_CLIENTID = "CLIENTID";
    private static final String AWS_ENCR_SECRET = "SECRET";
    private static final String AWS_SEARCH_ENDPT = "search-XYZ.cloudsearch.amazonaws.com";
    private static final String AWS_DOC_ENDPT = "doc-ABC.cloudsearch.amazonaws.com";

    public static void main(String args[]) {
        TestDocumentService service = new TestDocumentService();
        service.init();

        service.query();
    }

    private void init() {
        if(null==client) {
            log("Loading AWS Credentials");
            // AWSCredentials credentials = new ProfileCredentialsProvider().getCredentials();
            AWSCredentials credentials = new BasicAWSCredentials(getA(), getB());

            client = new AmazonCloudSearchClient(credentials);
            client.setSearchEndpoint(AWS_SEARCH_ENDPT);
            client.setDocumentEndpoint(AWS_DOC_ENDPT);
        }
    }

    public void query() {
        try {
            log("Creating New Query Document Request");
            AmazonCloudSearchQuery query = new AmazonCloudSearchQuery();
            query.queryParser = "lucene";
            query.query = "*";
            query.start = 0;
            query.size = 10000;
            query.setDefaultOperator("or");

            log("Querying Document from AWS CloudSearch");
            AmazonCloudSearchResult result = client.search(query);
            log("Query Complete! Total hits: " + result.found);
            for(Hit hit: result.hits) {
                log("Record.id: " + hit.id);
                // delete(hit.id);
            }
        } catch(Exception ex) {
            log("Exception in query()", ex);
        }
    }

    public void delete(String id) {
        try {
            log("Creating New Delete Document Request");
            AmazonCloudSearchDeleteRequest deleteRequest = new AmazonCloudSearchDeleteRequest();
            deleteRequest.id = id;
            deleteRequest.version = 1;

            log("Deleting Document from AWS CloudSearch");
            client.deleteDocument(deleteRequest);
            log("Complete!");
        } catch(Exception ex) {
            log("Exception in delete()", ex);
        }
    }

    public void upload() {
        try {
            log("Creating New Document Request");
            AmazonCloudSearchAddRequest addRequest = new AmazonCloudSearchAddRequest();
            addRequest.id = "acb123456";
            addRequest.version = 1;
            addRequest.addField("brand", "Some 2nd BRAND Value");
            addRequest.addField("category", "Some 2ndCATEGORY Value");
            addRequest.addField("content_text", "Some 2ndCONTENT_TEXT Value");
            addRequest.addField("coverage", "Some 2nd COVERAGE Value");
            addRequest.addField("headline", "Some 2nd HEADLINE Value");
            addRequest.addField("pagename", "Some 2nd PAGENAME Value");
            addRequest.addField("section", "Some 2ndS ECTION Value");
            addRequest.addField("uri", "Some 2nd URI Value");

            log("Adding Document to AWS CloudSearch");
            client.addDocument(addRequest);
            log("Complete!");

        } catch(Exception ex) {
            log("Exception in upload()", ex);
        }
    }

    private String getA() {
        return new String(DatatypeConverter.parseBase64Binary(AWS_ENCR_CLIENTID));
    }
    private String getB() {
        return new String(DatatypeConverter.parseBase64Binary(AWS_ENCR_SECRET));
    }

    public static void log(Object msg) {
        System.out.println("" + msg);
    }
    
    public static void log(Object msg, Exception ex) {
        log("" + msg);
        ex.printStackTrace();
    }
}
